import React from 'react'
import { StyleSheet, Text, View, StatusBar, SafeAreaView } from 'react-native';

import { Button, ButtonContainer } from '../components/Button'
import { Alert } from '../components/Alert'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#36b1f0',
        paddingHorizontal: 10
    },
    text: {
        color: '#fff',
        fontSize: 25,
        textAlign: 'center',
        letterSpacing: -0.02,
        fontWeight: '600'
    },
    safeareaview: {
        flex: 1,
        marginTop: 100,
        justifyContent: 'space-between'
    }
});

class Quiz extends React.Component {
    state = {
        correctCount: 0,
        totalCount: this.props.navigation.getParam("questions", []).length,
        activeQuestionIndex: 0,
        answerCorrect: false,
        answered: false
    }

    answer = (correct) => {
        this.setState(state => {
            const nextState = { answered: true };

            if (correct) {
                nextState.correctCount = state.correctCount + 1
                nextState.answerCorrect = true
            } else {
                nextState.answerCorrect = false
            }

            return nextState
        }, () => {
            setTimeout(() => {
                this.nextQuestion()
            }, 1000)
        })
    }

    nextQuestion = () => {
        this.setState(state => {
            let nextIndex = state.activeQuestionIndex + 1

            if (nextIndex >= state.totalCount) {
                nextIndex = 0
            }

            return {
                activeQuestionIndex: nextIndex,
                answered: false
            }
        })
    }

    render() {
        const colQuestions = this.props.navigation.getParam("questions", [])
        const q = colQuestions[this.state.activeQuestionIndex]

        return (
            <View style={[styles.container, { backgroundColor: this.props.navigation.getParam("color") }]}>
                <StatusBar barStyle="light-content" />

                <SafeAreaView style={styles.safeareaview}>
                    <View>
                        <Text style={styles.text}>{q.question}</Text>

                        <ButtonContainer>
                            {q.answers.map((a, i) => (
                                <Button key={i} text={a.text} onPress={() => this.answer(a.correct)} />
                            ))}
                        </ButtonContainer>
                    </View>

                    <Text style={styles.text}>{`${this.state.correctCount}/${this.state.totalCount}`}</Text>
                </SafeAreaView>
                <Alert correct={this.state.answerCorrect} visible={this.state.answered} />
            </View>
        )
    }
}

export default Quiz