import React, { Component } from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'

const screen = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        backgroundColor: 'green',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circle: {
        backgroundColor: '#ff4136',
        width: screen.width / 2,
        height: screen.width / 2,
        borderRadius: screen.width / 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        width: screen.width / 3,
    },
    circleCorrect: {
        backgroundColor: '#28a125'
    }
})

export const Alert = ({ correct, visible }) => {

    if (!visible) return null;

    const icon = correct ? require('../assets/check.png') : require('../assets/close.png')

    const circleStyles = [styles.circle]

    if (correct) {
        circleStyles.push(styles.circleCorrect)
    }

    return (
        <View style={styles.container}>
            <View style={circleStyles}>
                <Image source={icon} style={styles.icon} resizeMode='contain' />
            </View>
        </View>
    )
}