import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    row: {
        paddingHorizontal: 15,
        paddingVertical: 15,
        backgroundColor: '#36b1f0',
        marginBottom: 1
    },
    name: {
        color: '#FFF',
        fontSize: 18,
        fontWeight: '600'
    }
})

export const RowItem = ({ name, onPress, color }) => (
    <TouchableOpacity onPress={onPress} activeOpacity={0.8}>
        <View style={[styles.row, { backgroundColor: color }]}>
            <Text style={styles.name}>{name}</Text>
        </View>
    </TouchableOpacity>
)